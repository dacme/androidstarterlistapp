package com.spottedparrot.android.starterlistapp.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;

import com.spottedparrot.android.starterlistapp.db.DbModels.ItemColumns;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

/**
 * Provides access to a Meridian database of Loads, Journeys and Messages.
 *
 */
public class DbProvider extends ContentProvider {
    private static final String TAG = "DbProvider";
    private static final String DATABASE_NAME = "starterlistapp.db";

    // You must increase this version var
    // EVERY time a beta release is created. This allows for the db schema to be upgraded
    private static final int DATABASE_VERSION = 1;

    private static final String ITEMS_TABLE_NAME = "items";

    private static HashMap<String, String> sItemsProjectionMap;

    private static final int ITEMS = 1;
    private static final int ITEM_ID = 2;

    private static final String ITEMS_BASE_PATH = "items";

    public static final Uri CONTENT_URI_ITEM = Uri.parse("content://" + DbModels.AUTHORITY
            + "/" + ITEMS_BASE_PATH);

    private static final UriMatcher sUriMatcher;

    Context mContext = null;

    /**
     * This class helps open, create, and upgrade the database file.
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {

            // Define the List Items table:
            db.execSQL("CREATE TABLE " + ITEMS_TABLE_NAME + " ("
                    + ItemColumns._ID + " INTEGER PRIMARY KEY,"
                    + ItemColumns.content + " TEXT,"
                    + ItemColumns.description + " TEXT,"
                    + ItemColumns.image_path + " TEXT"
                    + ");");

            // TODO: add more db.execSQL lines for each of the other tables as needed.
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");

            db.execSQL("DROP TABLE IF EXISTS " + ITEMS_TABLE_NAME);

            // TODO: add more db.execSQL drop lines for each of the other tables as needed.

            onCreate(db);
        }

    }
    private DatabaseHelper mOpenHelper;

    @Override
    public boolean onCreate() {

        mContext = getContext();

        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();


        switch (sUriMatcher.match(uri)) {
            case ITEMS:
                qb.setTables(ITEMS_TABLE_NAME);
                qb.setProjectionMap(sItemsProjectionMap);
                break;

            case ITEM_ID:
                qb.setTables(ITEMS_TABLE_NAME);
                qb.setProjectionMap(sItemsProjectionMap);
                qb.appendWhere(ItemColumns._ID + "=" + uri.getPathSegments().get(1));
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        // If no sort order is specified use the default
        String orderBy;
        if (TextUtils.isEmpty(sortOrder)) {

            switch(sUriMatcher.match(uri)) {
                case ITEMS:
                case ITEM_ID:
                    orderBy = ItemColumns.DEFAULT_SORT_ORDER;
                    break;

                default:
                    throw new IllegalArgumentException("Unknown URI " + uri);
            }

        } else {
            orderBy = sortOrder;
        }

        // Get the database and run the query
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);

        Log.i(TAG, "Provider query returned: " + c.getCount() + " rows.");

        // Tell the cursor what uri to watch, so it knows when its source data changes
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;

    }

    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case ITEMS:
                return ItemColumns.CONTENT_TYPE;
            case ITEM_ID:
                return ItemColumns.CONTENT_ITEM_TYPE;


            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        // Validate the requested uri
        if (sUriMatcher.match(uri) != ITEMS) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }
        ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }

            SQLiteDatabase db = mOpenHelper.getWritableDatabase();

            long rowId;

            switch (sUriMatcher.match(uri)) {

                case ITEMS:
                    rowId = db.insert(ITEMS_TABLE_NAME, null, values);

                    if (rowId > 0) {
                        Uri itemUri = ContentUris.withAppendedId(ItemColumns.CONTENT_URI, rowId);
                        getContext().getContentResolver().notifyChange(itemUri, null);
                        return itemUri;
                    }
                    throw new SQLException("Failed to insert row into " + uri);
                    //break;

                default:
                    throw new SQLException("Failed to insert row into " + uri);
                    //break;

            }

    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {

            SQLiteDatabase db = mOpenHelper.getWritableDatabase();

            int count;
            switch (sUriMatcher.match(uri)) {

                case ITEMS:
                    count = db.delete(ITEMS_TABLE_NAME, where, whereArgs);
                    break;

                case ITEM_ID:
                    String itemId = uri.getPathSegments().get(1);
                    count = db.delete(ITEMS_TABLE_NAME, ItemColumns._ID + "=" + itemId
                            + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                    break;


                default:
                    throw new IllegalArgumentException("Unknown URI " + uri);
            }
            getContext().getContentResolver().notifyChange(uri, null);

            return count;

    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {


        SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        int count;
        switch (sUriMatcher.match(uri)) {

            case ITEMS:
                count = db.update(ITEMS_TABLE_NAME, values, where, whereArgs);
                break;

            case ITEM_ID:
                String itemId = uri.getPathSegments().get(1);
                count = db.update(ITEMS_TABLE_NAME, values, ItemColumns._ID + "=" + itemId
                        + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;

    }

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(DbModels.AUTHORITY, ITEMS_BASE_PATH, ITEMS);
        sUriMatcher.addURI(DbModels.AUTHORITY, ITEMS_BASE_PATH + "/#", ITEM_ID);

        // Item fields:
        sItemsProjectionMap = new HashMap<String, String>();
        sItemsProjectionMap.put(ItemColumns._ID, ItemColumns._ID);
        sItemsProjectionMap.put(ItemColumns.content, ItemColumns.content);
        sItemsProjectionMap.put(ItemColumns.description, ItemColumns.description);
        sItemsProjectionMap.put(ItemColumns.image_path, ItemColumns.image_path);

        // TODO: Create new ProjectionMaps for each new table in meridian (journeys/messages/webrequests

    }
}

